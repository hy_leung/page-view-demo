//
//  FSModelController.h
//  PageDemo
//
//  Created by Ho Yan Leung on 2013-08-09.
//  Copyright (c) 2013 ForeSee. All rights reserved.
//

#import <UIKit/UIKit.h>

@class FSDataViewController;

@interface FSModelController : NSObject <UIPageViewControllerDataSource>

- (FSDataViewController *)viewControllerAtIndex:(NSUInteger)index storyboard:(UIStoryboard *)storyboard;
- (NSUInteger)indexOfViewController:(FSDataViewController *)viewController;
-(int) dataCount;
-(void) addItem:(NSString*) itemName;
@end
