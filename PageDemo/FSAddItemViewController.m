//
//  FSAddItemViewController.m
//  PageDemo
//
//  Created by Ho Yan Leung on 2013-08-09.
//  Copyright (c) 2013 ForeSee. All rights reserved.
//

#import "FSAddItemViewController.h"

@interface FSAddItemViewController ()

@end

@implementation FSAddItemViewController
@synthesize delegate = _delegate;
@synthesize textField = _textField;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(IBAction)buttonClicked:(id)sender {
    [_delegate didAddItem:_textField.text];
}
@end
