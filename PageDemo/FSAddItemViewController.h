//
//  FSAddItemViewController.h
//  PageDemo
//
//  Created by Ho Yan Leung on 2013-08-09.
//  Copyright (c) 2013 ForeSee. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol FSAddItemViewControllerDelegate;
@interface FSAddItemViewController : UIViewController
@property (nonatomic,assign) id<FSAddItemViewControllerDelegate> delegate;
@property (nonatomic,assign) IBOutlet UITextField *textField;
-(IBAction)buttonClicked:(id)sender;
@end

@protocol FSAddItemViewControllerDelegate<NSObject>
-(void)didAddItem:(NSString*)itemText;
@end