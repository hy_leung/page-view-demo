//
//  FSDataViewController.m
//  PageDemo
//
//  Created by Ho Yan Leung on 2013-08-09.
//  Copyright (c) 2013 ForeSee. All rights reserved.
//

#import "FSDataViewController.h"

@interface FSDataViewController () {
}

@end

@implementation FSDataViewController
@synthesize delegate = _delegate;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.dataLabel.text = [self.dataObject description];
}

@end
