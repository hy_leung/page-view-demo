//
//  FSDataViewController.h
//  PageDemo
//
//  Created by Ho Yan Leung on 2013-08-09.
//  Copyright (c) 2013 ForeSee. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol FSDataViewControllerDelegate;
@interface FSDataViewController : UIViewController

@property (strong, nonatomic) IBOutlet UILabel *dataLabel;
@property (strong, nonatomic) id dataObject;
@property (strong, nonatomic) id<FSDataViewControllerDelegate> delegate;


@end

@protocol FSDataViewControllerDelegate<NSObject>

@end
    
