//
//  FSRootViewController.h
//  PageDemo
//
//  Created by Ho Yan Leung on 2013-08-09.
//  Copyright (c) 2013 ForeSee. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FSAddItemViewController.h"
@interface FSRootViewController : UIViewController <UIPageViewControllerDelegate, FSAddItemViewControllerDelegate>

@property (strong, nonatomic) UIPageViewController *pageViewController;
-(IBAction)toggleScrollingEnabled:(id)sender;
@end
